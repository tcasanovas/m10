# Transacciones (Multiusuari)

7. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2
```

> - user0: esborra tot el contingut de la taula punts
> - user0: insereix a la taula punts id = 70 i valor = 5
> - user1: comença transacció 
> - user1: esborra el contingut de la taula punts, perquè ell no veu que s'ha borrat el contingut.
> - user2: fa el count de la taula punts
> - Realment nomès s'aplicat el primer delete i el primer insert del user0
> - Resposta: count = 1 (el user2 està veient el contingut del user0)

8. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0
```

> - user0: insereix a la taula punts id = 80 i valor = 5
> - user0: insereix a la taula punts id = 81 i valor = 9
> - user1: comença transacció
> - user1: actualitza de la taula punts valor = 4 on id = 80
> - user2: comença transacció
> - user2: actualitza de la taula punts valor = 8 on id = 80
> - user1: actualitza de la taula punts valor = 10 on id = 81 (és queda bloquejat)
> - user2: actualitza de la taula punts valor = 6 on id = 80 (es queda bloquejat i es fa un DEADLOCK és a dir que es fa un rollback i només els canvis del user1 s'apliquen)
> - user2: aplica els canvis (no s'aplica cap canvi)
> - user1: aplica els canvis
> - Resposta: valor = 4

9. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
INSERT INTO punts (id, valor) VALUES (91,9); -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 91; -- Connexió 0
```

> - user0: inserim a la taula punts id = 90 i valor = 5
> - user1: comença transacció
> - user1: esborra tot el contingut de punts
> - user2: comença transacció
> - user2: insereix a la taula punts id = 91 i valor = 9
> - user2: aplica els canvis
> - user1: aplica els canvis
> - Resposta: No trobaria res perquè el element no existeix ja que el user1 és l'últim en fer commit i esborra el contingut de la taula punts.

La resposta seria algo aixi:
 valor 
-------
(0 rows)


10. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0
```
> - user0: insereix a la taula punts id = 100 valor = 5
> - user1: comença transacció
> - user1: insereix a la taula punts valor = 6 on id = 100
> - user2: comença transacció valor = 7 on id = 100 (deadlock perquè el user1 ja està fent servir aquest element)
> - user2: aplica els canvis (és a dir res)
> - user1: aplica els canvis
> - Resposta: valor = 6


11. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1
 
SELECT valor FROM punts WHERE id = 111; -- Connexió 0
```

> - user0: insereix a la taula punts id = 110 i valor = 5
> - user0: insereix a la taula punts id = 111 i valor = 5
> - user1: comença transacció
> - user1: actualitza la taula punts valor = 6 on id = 110
> - user2: comença transacció
> - user2: actualitza a la taula punts valor = 7 on id = 110 (bloquejat)
> - user2: actualitza a la taula punts valor = 7 on id = 111 (es queda en el buffer)
> - user2: fa savepoint anomenat a (es queda en el buffer)
> - user2: actualitza a la taula punts valor = 8 on id = 110 (es queda en el buffer)
> - user2: intenta fer rollback to a però està bloquejat (es queda en el buffer)
> - user2: aplica els canvis (es queda en el buffer)
> - user1: aplica ens canvis (desbloqueja al user2)
> - S'APLICA TOT EL QUE S'HAVIA QUEDAT EN EL BUFFER DEL USER2
> - Resposta: valor = 7

12. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0
```
> - user0: insereix a la taula punts id = 120 i valor = 5
> - user0: insereix a la taula punts id = 121 i valor = 5
> - user1: comença transacció
> - user1: actualitza a la taula punts valor = 6 on id = 121
> - user1: fa un savepoint anomenat a
> - user1: actualitza a la taula punts valor = 9 on id = 120
> - user2: comença transacció
> - user2: actualitza a la taula punts valor = 7 on id = 120 (bloquejat és queda en buffer perquè el user1 ja l'està modificant)
> - user1: Torna al savepoint a
> - user2: fa savepoint a
> - user2: actualitza a la taula punts valor = 8 on id = 120 (es queda al buffer tot i que l'element ja no s'està utilitzant perquè el user1 ha fet rollback to a)
> - user2: torna al safepoint a (es queda al buffer)
> - user2: aplica els canvis (es queda al buffer)
> - user1: aplica els canvis (desbloqueja al user1)
> - S'APLICA TOT LO DEL USER2
> Resposta: valor = 6s 