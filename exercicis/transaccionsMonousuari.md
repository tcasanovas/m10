Tomàs Casanovas Masegosa

# Funcions de transaccions (MONOUSUARI)

Creeu una base de dades anomenada __transaccions__, amb una única taula anomenada __punts__, amb la següent estructura: id de tipus INT (CP) i valor de tipus SMALLINT.

1. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  

```
INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;
SELECT valor FROM punts WHERE id = 10;
```  
> - Inserim valors id=10 valor=5
> - Començem transaccio
> - Actualitzem valor a 4 i id a 10
> - Fem rollback (desfem els canvis)
> - La resposta final es: valor = 5


2. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
```
INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;
```
> - Inserim valors id = 20 valor = 5
> - Començem transacció
> - Actualitzem valor = 4 i id = 20
> - Apliquem els canvis amb el commit
> - Resposta final: mostra valor = 4

3. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;
```
> - Inserim id = 30 i valor = 5
> - Començem transacció
> - Actualitzem valor = 4 i id = 30
> - Guardem el progrés però no l'apliquem (SAVEPOINT)
> - Inserim a la taula punts id = 31 valor = 7
> - Resposta: valor = 5

3b. 

```
INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor);
commit;
SELECT valor FROM punts WHERE id = 30;
```


4. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
> - S'elimina tot
> - inserim a la taula punts id = 40 valor = 5
> - començem transacció
> - actualitzem la taula punts valor = 4 on id = 40
> - guardem el estat l'anomenem a (SAVEPOINT)
> - inserim a la taula punts id = 41 valor = 7
> - tornem al punt a
> - contem les files de la taula punts el resultat és = 1


5. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;
```

> - inserim a la taula punts id = 50 valor = 5
> - començem transacció 
> - hi ha un error de codi, WHERE, no s'aplica el update
> - resposta: valor = 5

6. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```
> - Borrem tot de la taula punts
> - inserim a la taula punts id = 60 i valor = 5
> - començem transacció
> - actualitzem la taula punts valor = 4 id = 60
> - guardem estat A
> - inserim a punts id = 61 i valor = 8
> - guardem estat B
> - inserim a punts id = 61 i valor = 9 (ERROR perquè tenen el mateix ID, hauria de ser un UPDATE)
> - tornem al punt B
> - Apliquem canvis
> - resposta: sum(valor)=12 (8+4)