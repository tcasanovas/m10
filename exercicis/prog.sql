do $$
declare 
    num_empleats smallint;
begin
    --get num 
    select count(*) 
    INTO num_empleats 
    FROM emp;

    -- display a message
    raise notice 'Num empleats es %', num_empleats;
end $$;


--BD scott
-- funcio mostrar usuari, li paso com a a arg el codi empleat  
-- ma de sortit el seu nom 
CREATE OR REPLACE FUNCTION mostrar_user(p_empno smallint)
RETURNS varchar
AS $$ 
DECLARE
    v_ename varchar(50); 
    v_job varchar(50);
BEGIN
-- STRICT es per forçar l'error si no existeixen les dades
    SELECT ename, job INTO STRICT v_ename, v_job FROM emp WHERE empno = p_empno;

    RETURN 'El nom de l''empleat amb codi ' ||p_empno||' es '||v_ename|| ' i el seu job es: ' ||v_job;

    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 'No s''han trobat les dades '||p_empno;
    
END;
$$ LANGUAGE 'plpgsql';

-- desde la BD per provarala => SELECT mostrar_user(7788);