# Habilitar servidor postgresql per connexions remotes

L'usuari `postgres` el tenim tant de sistema operatiu com de base de dades.

- **usuari de sistema operatiu**: se li posa/canvia la contrassenya amb l'ordre `passwd`.

- **usuari de bases de dades**: l'usuari de BD es pot crear amb contrassenya o sense.  


 # Fitxers de configuració:

 - pg_hba.conf (`host-based authentication`)
 - postgresql.conf

Per saber on hi son, veure ús de l'ordre `locate`


Exemple:
~~~
create user XXX createdb with password ‘xxxx’;
create role XXX login createdb password ‘xxx’;
~~~
Si volem posar una contrassenya a posteriori,

    ALTER USER user_name WITH PASSWORD  'xxx';

Tant amb els mètodes `password` como el `md5` funcionen si l’usuari té contrassenya.


Cal que us agrupeu per parelles i cada alumne accedeixi mitjançant psql a la BD del seu company. Cal ensenyar al professor el sistema funcionant i fer un document on hi figurin els passos que cal realitzar.

# PASOS PER CONFIGURAR BASE DE DADES PER ACCES REMOT

## Des del servidor

```
En aquesta ruta també ens trobem el fitxer `pg_hba.conf`, necessari per dir usuaris,
IPs i mètodes d'autenticació al servidor.
La ruta canvia segons la distribució de GNU/Linux.

Per saber la ruta del fitxer de configuració, executem la següent ordre:

```
$ psql -U postgres template1 -c "SHOW config_file"
```

- Una vegada ja sabem la ruta, editem el fitxer postgresql.conf
  - Descomentar l'entrada _listen_addresses_ i possar-li el valor *
  - Descomentar el valor port
      listen_addresses = '*'
      port=5432

- Modificar el fitxer /var/lib/pgsql/data/pg_hba.conf i afegir una linia al l''apartat IPv4 local connections:

      host    all        all        192.168.0.83/32        trust

--    (D'aquesta manera nomes permetem l'acces a la base de dades al pc amb la ip
--      192.168.0.83. Ja que amb el que hem posat comprova a la direcció sencera. Si
--      en comptes de possar /32, possem /24 nomes comprobaria els 3 primers bits i
--      deixaria entrar a tota la xarxa)

- Fer restart del servei postgresql



Client


-- Ara ja podem entrar a la base de dades remotament

  psql -h pc43 -p 5432 -U postgres training

# PostgreSQL: How to reload config settings without restarting database

Option 1: From the command-line shell
su - postgres
/usr/bin/pg_ctl reload

Option 2: Using SQL
SELECT pg_reload_conf();

Using either option will not interrupt any active queries or connections to the database, thus applying these changes seemlessly.

